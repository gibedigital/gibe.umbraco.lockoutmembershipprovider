
create table umbracoUserLoginAttempts
(
	[user] int not null primary key,
	[failedPasswordAttempts] int not null,
	[failedPasswordAttemptWindowStart] datetime null
)
﻿using System;
using umbraco.BusinessLogic;
using umbraco.providers;

namespace Gibe.Umbraco
{
  public class LockoutMembershipProvider : UsersMembershipProvider
  {
    public override bool ValidateUser(string username, string password)
    {
      // we need to wrap this in a try/catch as passing a non existing 
      // user will throw an exception
      try
      {
        var user = new User(username);
        var attempts = new UserLoginAttempts(user.Id);

        if (user.Id != -1)
        {
          if (user.Disabled)
          {
            return false;
          }
          // If under the max number of attempts or over the max but also outside the window
          // allow the login attempt
          if (attempts.FailedPasswordAttempts < MaxInvalidPasswordAttempts ||
              (attempts.FailedPasswordAttempts >= MaxInvalidPasswordAttempts &&
              attempts.FailedPasswordAttempWindowStart < DateTime.Now.AddMinutes(-PasswordAttemptWindow)))
          {
            var success = user.ValidatePassword(EncodePassword(password));
            if (success)
            {
              // Reset the login attempts
              attempts.FailedPasswordAttempts = 0;
            }
            else
            {
              // Increment login attempts
              attempts.FailedPasswordAttempts++;
              if (attempts.FailedPasswordAttempWindowStart < DateTime.Now.AddMinutes(-PasswordAttemptWindow))
              {
                attempts.FailedPasswordAttempWindowStart = DateTime.Now;
              }
            }
            attempts.Update();
            return success;
          }
          return false;
        }
      }
      catch
      {
        // nothing to catch here - move on
      }

      return false;
    }
  }

  
}

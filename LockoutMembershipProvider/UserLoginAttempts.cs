﻿using System;
using umbraco.BusinessLogic;
using umbraco.DataLayer;

namespace Gibe.Umbraco
{
	/// <summary>
	/// Data for user login attempts
	/// </summary>
	public class UserLoginAttempts
	{
		public int _userId = -1;
		public int FailedPasswordAttempts { get; set; }
		public DateTime FailedPasswordAttempWindowStart { get; set; }

		public UserLoginAttempts(int userId)
		{
			_userId = userId;

			using (var dr = SqlHelper.ExecuteReader(
					"Select failedPasswordAttempts, failedPasswordAttemptWindowStart from umbracoUserLoginAttempts where [user] = @userId",
					SqlHelper.CreateParameter("@userId", _userId)))
			{
				if (dr.Read())
				{
					FailedPasswordAttempts = dr.Get<Int32>("failedPasswordAttempts");
					FailedPasswordAttempWindowStart = dr.Get<DateTime>("failedPasswordAttemptWindowStart");
				}
				else
				{
					FailedPasswordAttempts = 0;
					FailedPasswordAttempWindowStart = DateTime.Now;

					SqlHelper.ExecuteNonQuery("Insert into umbracoUserLoginAttempts([user], [failedPasswordAttempts], [failedPasswordAttemptWindowStart]) values(@userId, 0, @date)",
						SqlHelper.CreateParameter("@userId", _userId),
						SqlHelper.CreateParameter("@date", FailedPasswordAttempWindowStart));
				}
			}
		}

		public void Update()
		{
			SqlHelper.ExecuteNonQuery("Update umbracoUserLoginAttempts set [failedPasswordAttempts] = @attempts, [failedPasswordAttemptWindowStart] = @date where [user] = @userId",
						SqlHelper.CreateParameter("@userId", _userId),
						SqlHelper.CreateParameter("@attempts", FailedPasswordAttempts),
						SqlHelper.CreateParameter("@date", FailedPasswordAttempWindowStart));

		}

		private static ISqlHelper SqlHelper
		{
			get { return Application.SqlHelper; }
		}

	}
}
